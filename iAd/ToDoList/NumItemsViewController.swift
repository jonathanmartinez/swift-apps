//
//  NumItemsViewController.swift
//  ToDoList
//
//  Created by Domingo on 12/5/15.
//  Copyright (c) 2015 Universidad de Alicante. All rights reserved.
//

import UIKit
import iAd

class NumItemsViewController: UIViewController {

    var terminados = 0
    @IBOutlet weak var numItems: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.canDisplayBannerAds = true
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        let store = NSUbiquitousKeyValueStore.defaultStore()
        store.setLongLong(Int64(terminados), forKey: "numItemsDone")
        super.viewWillAppear(animated)
        
        //numItems.text = "Se han completado \(terminados) ítems."
        
        //let store = NSUbiquitousKeyValueStore.defaultStore()
        let itemsStore = store.longLongForKey("numItemsDone")
        if (itemsStore > 0) {
            terminados = Int(itemsStore)
            println("Leído de iCloud: \(terminados) ítems terminados")
            numItems.text = "Se han completado \(terminados) ítems."
        } else {
            println("Sin valor en iCloud de ítems terminados")
            numItems.text = "Sin valor en iCloud de ítems terminados."
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
