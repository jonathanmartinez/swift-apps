//
//  CalculatorBrain.swift
//  Calculadora
//
//  Created by Máster Móviles on 24/04/15.
//  Copyright (c) 2015 Domingo Gallardo. All rights reserved.
//

import Foundation

class CalculatorBrain {
    private enum Op {
        case Operand(Double)
        case UnaryOperation(String, Double -> Double)
        case BinaryOperation(String, (Double, Double) -> Double)
        func toString() -> String {
            switch self {
            case .Operand(let operand):
                return "\(operand)"
            case .UnaryOperation(let opString, _):
                return opString
            case .BinaryOperation(let opString, _):
                return opString
            }
        }
    }
    
    private var opStack = [Op]()
    private var knownOps = [String: Op]()
    
    init(){
        knownOps["x"] = Op.BinaryOperation("x", *)
        knownOps["÷"] = Op.BinaryOperation("÷") { $1 / $0 }
        knownOps["+"] = Op.BinaryOperation("+", +)
        knownOps["-"] = Op.BinaryOperation("-") { $1 / $0 }
        knownOps["√"] = Op.UnaryOperation("√", sqrt)
        
    }
    
    private func evaluate(ops: [Op]) -> (result: Double?, remainingOps: [Op]) {
        if !ops.isEmpty{
            var remainingOps = ops
            let op = remainingOps.removeLast()
            switch op{
            case .Operand(let operand):
                return (operand, remainingOps)
            case .UnaryOperation(_, let operation):
                let operandEvaluation = evaluate(remainingOps)
                if let operand = operandEvaluation.result {
                    return (operation(operand), operandEvaluation.remainingOps)
                }
            case .BinaryOperation(_, let operation):
                let op1Evaluation = evaluate(remainingOps)
                if let operand1 = op1Evaluation.result{
                    let op2Evaluation = evaluate(op1Evaluation.remainingOps)
                    if let operand2 = op2Evaluation.result {
                        return (operation(operand1,operand2), op2Evaluation.remainingOps)
                    }
                }
                
            }
        }
        return (nil, ops)
    }
    
    private func toString(ops: [Op]) -> String {
        var opsStrings = "["
        var first = true
        for ops in opStack {
            if !first {
                opsStrings += ", "
            }
            opsStrings += ops.toString()
            first = false
        }
        opsStrings += "]"
        return opsStrings
    }
    
    func evaluate() -> Double? {
        let (result, remainder) = evaluate(opStack)
        println("Stack: " + toString(opStack))
        println("Result: \(result)")
        return result
    }
    
    func pushOperand(operand: Double) -> Double? {
        opStack.append(Op.Operand(operand))
        return evaluate()
    }
    
    func performOperation(symbol: String) -> Double? {
        if let operation = knownOps[symbol]{
            opStack.append(operation)
        }
        return evaluate()
    }
}
