//
//  ViewController.swift
//  Mapas
//
//  Created by Máster Móviles on 29/05/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

import UIKit


class imageDetail: UIViewController {
    
    var thumbImage: UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
        imageInView.image = thumbImage
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var imageInView: UIImageView!
}

