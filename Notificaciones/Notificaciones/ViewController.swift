//
//  ViewController.swift
//  Notificaciones
//
//  Created by Máster Móviles on 15/05/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func botonPulsado(sender: AnyObject) {
        let localNotification:UILocalNotification = UILocalNotification()
        localNotification.alertAction = "Volver a la app"
        localNotification.alertBody = "¡¡Funciona!!"
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 10)
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
    }

}

