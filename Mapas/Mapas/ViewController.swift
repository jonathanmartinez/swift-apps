//
//  ViewController.swift
//  Mapas
//
//  Created by Máster Móviles on 29/05/15.
//  Copyright (c) 2015 Máster Móviles. All rights reserved.
//

import UIKit
import MapKit

class Pin:  NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String!
    var subtitle: String!
    var thumbImage: UIImage
    
    init(num: Int, coordinate: CLLocationCoordinate2D) {
        self.title = "Pin \(num)"
        self.subtitle = "Un bonito lugar"
        self.coordinate = coordinate
        if (num % 2 == 0) {
            self.thumbImage = UIImage(named: "alicante1_thumb.png")!
        } else {
            self.thumbImage = UIImage(named: "alicante2_thumb.png")!
        }
        super.init()
    }
}

class ViewController: UIViewController, MKMapViewDelegate {
    
    
    
    var cont = 1;
    
    enum TipoMapa: Int {
        case Mapa = 0
        case Satélite
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let userTrackingButton = MKUserTrackingBarButtonItem(mapView: mapView)
        self.navigationItem.leftBarButtonItem = userTrackingButton
        /*let map = MKMapView(frame: CGRectMake(0, 30, 320, 200))
        self.view.addSubview(map)
        map.delegate = self*/
        
    }
    
    @IBOutlet weak var mapView: MKMapView! {
        didSet {
            mapView.mapType = .Standard
            mapView.delegate = self
            let alicanteLocation =
            CLLocationCoordinate2D(latitude: 38.3453,
                longitude: -0.4831)
            let initialLocation =
            CLLocation(latitude: alicanteLocation.latitude,
                longitude: alicanteLocation.longitude)
            centerMapOnLocation(mapView, loc: initialLocation)
        }
    }
    
    func centerMapOnLocation(mapView: MKMapView, loc: CLLocation) {
        let regionRadius: CLLocationDistance = 1000
        let coordinateRegion =
        MKCoordinateRegionMakeWithDistance(loc.coordinate,
            regionRadius * 4.0, regionRadius * 4.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    
    @IBAction func seleccion(sender: UISegmentedControl) {
        let tipoMapa = TipoMapa(rawValue: sender.selectedSegmentIndex)
        switch (tipoMapa!) {
        case .Mapa:
            mapView.mapType = MKMapType.Standard
        case .Satélite:
            mapView.mapType = MKMapType.Satellite
        }
    }
    
    @IBAction func addPin(sender: AnyObject) {
        let pin = Pin(num: cont, coordinate: mapView.centerCoordinate)
        mapView.addAnnotation(pin)
        cont++;
    }
    

    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if let pin = annotation as? Pin{
            var view: MKPinAnnotationView
            
            if let dequedView = mapView.dequeueReusableAnnotationViewWithIdentifier("PinAnnotationView") as? MKPinAnnotationView {
                dequedView.annotation = annotation
                view = dequedView
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "PinAnnotationView")
                view.pinColor = MKPinAnnotationColor.Red
                view.animatesDrop = true
                view.canShowCallout = true
            }
            
            
            let pin = annotation as Pin
            let thumbnailImageView = UIImageView(frame: CGRect(x:0, y:0, width: 59, height: 59))
            thumbnailImageView.image = pin.thumbImage
            view.leftCalloutAccessoryView = thumbnailImageView
            view.rightCalloutAccessoryView = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as UIButton
            
            return view
        }
        else{
            return nil
        }
       
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {
        performSegueWithIdentifier("DetalleImagen", sender: view)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "DetalleImagen" {
            if let pin = (sender as? MKAnnotationView)?.annotation as? Pin {
                if let vc = segue.destinationViewController as? imageDetail {
                    vc.thumbImage = pin.thumbImage
                }
            }
        }
    }

}

